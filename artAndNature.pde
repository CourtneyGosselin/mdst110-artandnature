/* Art and Nature
*  Courtney Gosselin
*  MDST 110
*  October 19, 2020
*/

/*
* References
* http://solemone.de/demos/snow-effect-processing/
* https://forum.processing.org/two/discussion/9526/falling-snowflakes
* https://www.openprocessing.org/sketch/118960/
* https://processing.org/tutorials/objects/
* https://processing.org/reference/rotate_.html
*/

PImage windowLeaves;
PImage windowFrame;
PImage sky;
PImage landscape;
int quantity = 30;
float wind = 0;
float airRes;
int minleafSize = 20;
int maxleafSize = 40;
Leaf [] Leaves = new Leaf[quantity];
Cloud Cloud;

// Leaf object that contains wind modefier, leaf size, leaf image, and leaf location.
class Leaf {
  PImage image;
  int size;
  float xPosition;
  float yPosition;
  float windMod;

  Leaf() {
    windMod = 1;
    size = round(random(minleafSize, maxleafSize));
    
    xPosition = random(100, width-100);
    yPosition = random(-700, -50);
    
    switch(floor(random(0, 4))) {
    case 0:
      image = loadImage("leaf.png");
      break;
    case 1:
      image = loadImage("leaf2.png");
      break;
    case 2:
      image = loadImage("leaf3.png");
      break;
    case 3:
      image = loadImage("leaf4.png");
      break;
    default:
      image = loadImage("leaf.png");
    }
    
    image.resize(size,size);
    
  }
}

// Cloud object which contains the coud image and position in the x axis
class Cloud {
  PImage cloud;
  float cloudPos;

  Cloud() {
    cloud = loadImage("clouds.png");
    cloudPos = -1000;
  }
}

void setup() {
  size(800, 800);
  frameRate(60);
  noStroke();
  smooth();

  // Images used to create background and foreground
  sky = loadImage("background.png");
  windowLeaves = loadImage("windowleaves.png");
  windowFrame = loadImage("windowframe.png");
  landscape = loadImage("mountain.png");
  
  // Wind intialization 
  while (abs(wind)<1) {
    wind = random(-3, 3);
  }
  // Air resistance set
  airRes = 0.12;
  
  // Objects set
  Cloud = new Cloud();
  
  for (int i = 0; i < quantity; i++) {
    Leaves[i] = new Leaf();
    
  }
}

void draw() {

  background(sky);
  // Setting the clouds position 
  image(Cloud.cloud, Cloud.cloudPos, 0);
  Cloud.cloudPos += 0.4;
  if (Cloud.cloudPos>width) { // Resetting position when the image moves off screen
    Cloud.cloudPos = -1600;
  }
  image(landscape, 0, 0);

  for (int i = 0; i < Leaves.length; i++) {
    // Setting each leaves position
    image(Leaves[i].image, Leaves[i].xPosition, Leaves[i].yPosition);
    
    // Changes to wind overtime
    Leaves[i].windMod *= random(0.92, 1.08);
    wind += random(-0.08, 0.08) + wind/5;
    while (wind>3) {
      wind-=0.08;
    }
    while (wind<-3) {
      wind += 0.08;
    }

    // Changes to air resistance overtime
    airRes += random(-0.002, 0.002);
    while (airRes>0.22) {
      airRes-=0.02;
    }
    while (airRes<0.07) {
      airRes += 0.02;
    }
    
    
    Leaves[i].xPosition += wind*Leaves[i].windMod;

    Leaves[i].yPosition += airRes*Leaves[i].size; 
    
    // Respawning leaves when they leave the screen
    if (Leaves[i].xPosition > width + 1000 + Leaves[i].size || Leaves[i].xPosition < -Leaves[i].size - 300 || Leaves[i].yPosition > height + 200 + Leaves[i].size) {
      Leaves[i].xPosition = random(0, width);
      Leaves[i].yPosition = -Leaves[i].size;
      Leaves[i].windMod = 1;
    }
  }

  image(windowLeaves, 0, 0);
  image(windowFrame, 0, 0);
}
